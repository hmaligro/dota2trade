﻿using System;
using D2TP.Commons.Enumerations.Entities.Notification;

namespace D2TP.Notification.Entities
{
    /// <summary>
    /// Abstract class that handle the request for each created Job.
    /// </summary>
    public abstract class Request : Models.Notification
    {

        /// <summary>
        /// Gets or sets the subject/title of the created request.
        /// </summary>
        public abstract new string Subject { get; set; }

        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public abstract new string ID { get; set; }

        /// <summary>
        /// Gets or sets the date the request is created.
        /// </summary>
        public abstract new DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the priority of request.
        /// { High, Normal }
        /// </summary>
        public abstract new Priority MessagingPriority { get; set; }

        /// <summary>
        /// Gets or sets the messaging type of the request
        /// { Email, Sms, Alarm }
        /// </summary>
        public abstract new Messaging Type { get; set; }

        /// <summary>
        /// Gets or sets the content of the created request
        /// </summary>
        public abstract new string BodyHtml { get; set; }

        /// <summary>
        /// Gets or sets sender of the created request.
        /// </summary>
        public abstract new string From { get; set; }

        /// <summary>
        /// Gets or sets receiver of the created request.
        /// </summary>
        public abstract new string To { get; set; }

    }

    /// <summary>
    /// Class that handles sms messages request from client.
    /// </summary>
    public class SmsRequest : Request
    {
        /// <summary>
        /// Gets or sets the content of the created request
        /// </summary>
        public override string BodyHtml { get; set; }
    
        /// <summary>
        /// Gets or sets the date the request is created.
        /// </summary>
        public override DateTime DateCreated { get; set; }
     
        /// <summary>
        /// Gets or sets sender of the created request.
        /// </summary>
        public override string From { get; set; }
     
        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public override string ID { get; set; }
     
        /// <summary>
        /// Gets or sets the priority of request.
        /// { High, Normal }
        /// </summary>
        public override Priority MessagingPriority { get; set; }
     
        /// <summary>
        /// Gets or sets the subject/title of the created request.
        /// </summary>
        public override string Subject { get; set; }

        /// <summary>
        /// Gets or sets receiver of the created request.
        /// </summary>
        public override string To { get; set; }
      
        /// <summary>
        /// Gets or sets the messaging type of the request
        /// { Email, Sms, Alarm }
        /// </summary>
        public override Messaging Type { get; set; }
    }

    /// <summary>
    /// Class that handles mail message request from client.
    /// </summary>
    public class MailRequest : Request
    {

        /// <summary>
        /// Gets or sets the carbon copy.
        /// </summary>
        public new string Cc { get; set; }

        /// <summary>
        /// Gets or sets the bcc of the created request.
        /// </summary>
        public new string Bcc { get; set; }

        /// <summary>
        /// Gets or sets the display name of sender.
        /// </summary>
        public new string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the date the request is created.
        /// </summary>
        public override DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the priority of request.
        /// { High, Normal }
        /// </summary>
        public override Priority MessagingPriority { get; set; }

        /// <summary>
        /// Gets or sets the messaging type of the request
        /// { Email, Sms, Alarm }
        /// </summary>
        public override Messaging Type { get; set; }

        /// <summary>
        /// Gets or sets the content of the created request
        /// </summary>
        public override string BodyHtml { get; set; }

        /// <summary>
        ///  Gets or sets sender of the created request.
        /// </summary>
        public override string From { get; set; }

        /// <summary>
        /// Gets or sets receiver of the created request.
        /// </summary>
        public override string To { get; set; }

        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public override string ID { get; set; }

        /// <summary>
        /// Gets or sets the subject/title of the created request.
        /// </summary>
        public override string Subject { get; set; }
    }

}
