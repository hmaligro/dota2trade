﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// Initializes any types of d2tp.notification jobs.
    /// Mainly used for unit testing.
    /// </summary>
    public static class JobFactory<T>
        where T : NotificationRequest
    {
        /// <summary>
        /// Sends request manually without using any trigger.
        /// </summary>
        /// <returns></returns>
        public static bool SendJobNotification(T request)
        {
            var job = new JobNotification() { Request = request };
            var response = job.Send(request);
            return response.IsOK;
        }

    }
}
