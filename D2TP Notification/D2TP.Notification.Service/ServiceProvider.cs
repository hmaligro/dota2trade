﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// Class that instantiates a Scheduler instance for Jobs with simple trigger and cron trigger execution. Implements an IBuilder  interface.
    /// Real-time notification uses Simple Trigger, which triggers a single Job for sending notification signaled from API.
    /// Scheduled reporting uses Cron Trigger, which triggers a scheduled send notifications for reports.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class ServiceProvider<T> : IService<T>
          where T : JobBase<NotificationRequest>
    {
        private Quartz.IScheduler _scheduler = null;

        /// <summary>
        /// Creates a scheduler service provider based on the job required.
        /// </summary>
        /// <returns></returns>
        public static IService<T> CreateService()
        {
            return new ServiceProvider<T>();
        }

        public ServiceProvider()
        {
            ISchedulerFactory factory = new StdSchedulerFactory();

            _scheduler = factory.GetScheduler();
        }

        /// <summary>
        /// Gets if scheduler instance has been started.
        /// </summary>
        public bool IsStarted
        {
            get
            {
                return _scheduler != null && _scheduler.IsStarted;
            }
        }

        /// <summary>
        /// Gets if scheduler instance has been stop.
        /// </summary>
        public bool IsShutdown
        {
            get
            {
                return _scheduler != null && _scheduler.IsShutdown;
            }
        }

        /// <summary>
        /// Starts the scheduler.
        /// </summary>
        /// <param name="delay"></param>
        public void Start(long delay)
        {
            if (_scheduler != null)
                _scheduler.StartDelayed(new TimeSpan(delay));
        }

        /// <summary>
        /// Shutdowns the scheduler.
        /// </summary>
        public void Stop()
        {
            if (_scheduler != null)
                _scheduler.Shutdown();
        }

        /// <summary>
        /// Creates a job to the scheduler.
        /// </summary>
        /// <param name="job"></param>
        public void CreateJob(T job)
        {
            // Defines the job property data mapped object.
            var map = new Dictionary<string, object>() { { "Request", job.Request } };
            var jobData = new JobDataMap((IDictionary<string, object>)map);

            // Define the job and tie it to our HelloJob class
            var j = JobBuilder.Create<T>()
                .WithIdentity(job.ID, typeof(T).ToString())
                .UsingJobData(jobData)
                .Build();

            // Trigger the job to run now, and then repeat every 40 seconds
            var t = TriggerBuilder.Create()
                .WithIdentity("trigger_for_" + job.ID, typeof(T).ToString())
                .WithPriority((int)job.Request.MessagingPriority)
                .StartNow()
                .WithSchedule(job.Schedule)
                .Build();

            // Tell Quartz to schedule jobs using triggers
            _scheduler.ScheduleJob(j, t);
        }

        /// <summary>
        /// Returns TRUE if job already existing in scheduler.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool JobExist(string name)
        {
            return _scheduler.GetJobDetail(new JobKey(name)) != null;
        }

    }
}
