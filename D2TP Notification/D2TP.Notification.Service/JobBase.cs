﻿using System;
using System.Net.Mail;
using System.Text;
using D2TP.Commons.Enumerations.Entities.Notification;
using D2TP.Commons.Entities.Http;
using Quartz;
using D2TP.Configuration;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// An abstract class that handles Job notification. Implements the interface Quartz.IJob. 
    /// Implements the IMessenger interface.
    /// </summary>
    public abstract class JobBase<T> : IJob, IMessenger<T>
         where T : NotificationRequest
    {

        /// <summary>
        /// Gets or sets the unique identifier of a job.
        /// </summary>
        public abstract string ID { get; set; }

        /// <summary>
        /// An abstract method that executes Job action.
        /// </summary>
        /// <param name="context"></param>
        public virtual void Execute(IJobExecutionContext context)
        {
            object request = null;

            var dataMap = context.JobDetail.JobDataMap;

            if (dataMap.TryGetValue("Request", out request)
                && request != null)
            {
                Send((T)request);
            }
            else
            {
                // TODO: Logs here "Failed to get the desired request object."
            }
        }

        /// <summary>
        /// Gets and sets the request instance of the job.
        /// </summary>
        public abstract T Request { get; set; }

        /// <summary>
        /// Gets or sets the SimpleScheduleBuilder instance of a job.
        /// </summary>
        public abstract SimpleScheduleBuilder Schedule { get; set; }

        /// <summary>
        /// Sends request based on the type of message.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Response Send(T request)
        {
            var response = new NotificationResponse();

            try
            {
                var subject = request.Subject;
                var body = request.BodyHtml;
                var to = request.To;
                var from = request.From;

                switch (request.Type)
                {
                    case Messaging.Email:
                        {
                            var networkCredentials = new System.Net.NetworkCredential(ConfigurationFactory.SmtpUsername, ConfigurationFactory.SmtpPassword, ConfigurationFactory.SmtpDomain);

                            using (var smtp = new SmtpClient(ConfigurationFactory.SmtpServer)
                            {
                                UseDefaultCredentials = false,
                                Credentials = networkCredentials,
                                Timeout = ConfigurationFactory.SmtpTimeout,
                                EnableSsl = ConfigurationFactory.SmtpEnableSsl,
                                Port = ConfigurationFactory.SmtpPort
                            })
                            {
                                using (var msg = new MailMessage())
                                {
                                    switch (request.MessagingPriority)
                                    {
                                        case Priority.High:
                                            msg.Priority = MailPriority.High; break;
                                        default:
                                            msg.Priority = MailPriority.Normal; break;
                                    }

                                    msg.From = new MailAddress(from, (request as MailRequest).DisplayName);
                                    msg.To.Add(new MailAddress(to));
                                    msg.BodyEncoding = Encoding.UTF8;
                                    msg.Subject = subject;
                                    msg.IsBodyHtml = true;
                                    msg.Body = body;

                                    smtp.Send(msg);
                                }
                            }

                            break;
                        }
                    case Messaging.SMS:
                        {
                            throw new NotImplementedException();
                            // TODO : Code to send sms.
                        }
                    default:
                        {
                            break;
                        }

                }

                response.IsOK = true;
                response.DateSent = DateTime.Now;
                response.Message = string.Empty;
            }
            catch (Exception ex)
            {
                response.IsOK = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
