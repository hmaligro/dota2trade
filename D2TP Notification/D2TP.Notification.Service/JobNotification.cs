﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// A class that inherits JobBase and handles sending email from Notification model
    /// </summary>
    internal class JobNotification : JobBase<NotificationRequest>
    {
        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        public override string ID { get; set; }

        /// <summary>
        /// Gets and sets the request for the job.
        /// </summary>
        public override NotificationRequest Request { get; set; }

        /// <summary>
        /// Gets or sets the SimpleScheduleBuilder instance of a job.
        /// </summary>
        public override SimpleScheduleBuilder Schedule { get; set; }

        /// <summary>
        /// A method that executes Job action.
        /// </summary>
        /// <param name="context"></param>
        public override void Execute(IJobExecutionContext context)
        {
            base.Execute(context);
        }
    }
}
