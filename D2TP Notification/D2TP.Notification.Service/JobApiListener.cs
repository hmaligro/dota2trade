﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D2TP.Commons.Entities.Http;
using Quartz;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// A class that inherits JobBase and handles api call to request notifications.
    /// </summary>
    internal class JobApiListener : JobBase<NotificationRequest>
    {
        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        public override string ID { get; set; }

        /// <summary>
        /// Gets and sets the request for the job.
        /// </summary>
        public override NotificationRequest Request { get; set; }

        /// <summary>
        /// Gets or sets the SimpleScheduleBuilder instance of a job.
        /// </summary>
        public override SimpleScheduleBuilder Schedule { get; set; }

        /// <summary>
        /// A method that executes Job action.
        /// Requests from Notification API for new records.
        /// </summary>
        /// <param name="context"></param>
        public override void Execute(IJobExecutionContext context)
        {
            // TODO: Codes to access API to get notification records here.
        }
    }
}
