﻿using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    public interface IService<T> 
        where T : JobBase<NotificationRequest>
    {
        /// <summary>
        /// Returns TRUE if job already existing in scheduler.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool JobExist(string name);

        /// <summary>
        /// Gets if scheduler instance has been started.
        /// </summary>
        bool IsStarted { get; }

        /// <summary>
        /// Gets if scheduler instance has been stop.
        /// </summary>
        bool IsShutdown { get; }

        /// <summary>
        /// Starts the scheduler.
        /// </summary>
        void Start(long delay);

        /// <summary>
        /// Shutdowns the scheduler.
        /// </summary>
        void Stop();

        /// <summary>
        /// Creates a job to the scheduler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        void CreateJob(T job);
    }
}
