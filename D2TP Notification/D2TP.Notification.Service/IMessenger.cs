﻿using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    public interface IMessenger<T>
        where T : NotificationRequest
    {
      
        /// <summary>
        ///  Sends the request based on the type of notification.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Response Send(T request);

    }
}
