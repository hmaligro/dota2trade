﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Notification.Service
{
    public interface INeedInitialization : IDisposable
    {
        /// <summary>
        /// Initialization of all members will be implemented inside this method.
        /// </summary>
        void Init();
    }
}
