﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// Class that uses NotificationBuilder to build Jobs.
    /// </summary>
    public class ServiceManager : INeedInitialization
    {
        private static ServiceManager _instance;
        /// <summary>
        /// Gets the singleton instance of ServiceManager class.
        /// </summary>
        public static ServiceManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ServiceManager();
                return _instance;
            }
        }

        private IService<JobNotification> NotificationProvider { get; set; }
        private IService<JobApiListener> ApiListenerProvider { get; set; }

        private ServiceManager()
        {
            NotificationProvider = ServiceProvider<JobNotification>.CreateService();
            NotificationProvider.Start(0);

            ApiListenerProvider = ServiceProvider<JobApiListener>.CreateService();
            ApiListenerProvider.Start(0);
        }

        /// <summary>
        /// Implements all initialization for service manager instance.
        /// </summary>
        public void Init() 
        {
            // Create a job that request notification record from API.
            var apiListener = new JobApiListener();
            TriggerFactory<JobApiListener>.AssignJobSchedule(ref apiListener, 1, true);
            ApiListenerProvider.CreateJob(apiListener);
        }

        /// <summary>
        /// Disposes and stops all service provider instances.
        /// </summary>
        public void Dispose()
        {
            if (ApiListenerProvider != null)
                ApiListenerProvider.Stop();

            if (NotificationProvider != null)
                NotificationProvider.Stop();
        }
    }
}
