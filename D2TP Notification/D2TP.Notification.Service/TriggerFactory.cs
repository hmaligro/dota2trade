﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service
{
    /// <summary>
    /// Class that handles creation and schedules behind each trigger.
    /// </summary>
    public class TriggerFactory<T>
        where T : JobBase<NotificationRequest>
    {

        /// <summary>
        /// Builds a scheduled trigger for jobs.
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        public static void AssignJobSchedule(ref T job, int days, bool? repeat = null)
        {
            var schedule = SimpleScheduleBuilder.Create();
            schedule = schedule.WithIntervalInHours(days * 24);

            if (repeat.HasValue && repeat.Value)
                schedule = schedule.RepeatForever();

            job.Schedule = schedule;
        }


    }
}
