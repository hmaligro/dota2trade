﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeItEasy;
using D2TP.Commons.Entities.Http;

namespace D2TP.Notification.Service.Test
{
    /// <summary>
    /// Summary description for IBuilder_TestClass
    /// </summary>
    [TestClass]
    public class Fake_IService_TestClass
    {
        public Fake_IService_TestClass()
        {

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private IService<JobBase<NotificationRequest>> _service;

        [TestInitialize()]
        public virtual void IService_TestInitialize()
        {
            // A scheduler is instantiated.
            _service = A.Fake<IService<JobBase<NotificationRequest>>>();
        }

        [TestMethod]
        public virtual void Start_NotificationBuilder()
        {
            long delay = 0;
            // The scheduler is started and ready to accept job and triggers.
            A.CallTo(() => _service.Start(delay)).MustHaveHappened(Repeated.NoMoreThan.Once);
            // Builder is started property equals to TRUE
            A.CallTo(() => _service.IsStarted).Returns(true);
        }

        [TestMethod]
        public virtual void Stop_NotificationBuilder()
        {
            // The schuduler is shutdown and never can accept any new jobs.
            A.CallTo(() => _service.Stop()).MustHaveHappened(Repeated.NoMoreThan.Once);
            // Builder is shutdown property equals to TRUE
            A.CallTo(() => _service.IsShutdown).Returns(true);
            // A call to create job must not happen when builder is shutdown.
            A.CallTo(() => _service.CreateJob(A.Fake<JobBase<NotificationRequest>>())).MustNotHaveHappened();
        }

        [TestMethod]
        public virtual void CreateJob_To_NotificationBuilder()
        {
            long delay = 0;
            // Scheduler is started before creating jobs.
            A.CallTo(() => _service.Start(delay)).MustHaveHappened(Repeated.NoMoreThan.Once); ;
            // Scheduler can create job when it is started.
            A.CallTo(() => _service.CreateJob(A.Fake<JobBase<NotificationRequest>>())).DoesNothing();
        }
    }
}
