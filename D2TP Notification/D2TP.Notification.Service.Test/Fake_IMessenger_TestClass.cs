﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using D2TP.Commons.Entities.Http;
using FakeItEasy;

namespace D2TP.Notification.Service.Test
{
    /// <summary>
    /// Summary description for IMessenger_TestClass
    /// </summary>
    [TestClass]
    public class Fake_IMessenger_TestClass
    {
        public Fake_IMessenger_TestClass()
        {

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private IMessenger<NotificationRequest> _mail;

        [TestInitialize]
        public virtual void IMessenger_TestInitialize()
        {
            _mail = A.Fake<IMessenger<NotificationRequest>>();
        }

        [TestMethod]
        public virtual void SendEmail_By_SmtpClient()
        {
            var request = A.Fake<NotificationRequest>();
            A.CallTo(() => _mail.Send(request)).Returns<Response>(new NotificationResponse() { DateSent = DateTime.Now, IsOK = true, Message = string.Empty });
        }
    }
}
