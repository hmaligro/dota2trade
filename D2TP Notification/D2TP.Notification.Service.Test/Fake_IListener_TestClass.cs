﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeItEasy;

namespace D2TP.Notification.Service.Test
{
    /// <summary>
    /// Summary description for IListener_TestClass
    /// </summary>
    [TestClass]
    public class Fake_INeedInitialization_TestClass
    {
        public Fake_INeedInitialization_TestClass()
        {

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private INeedInitialization _listener;

        [TestInitialize]
        public virtual void INeedInitialization_TestInitialize()
        {
            // A service manager is instantiated.
            _listener = A.Fake<INeedInitialization>();
        }

        [TestMethod]
        public virtual void Init_Service_Providers()
        {
            // Initialized service providers and job creation.
            A.CallTo(() => _listener.Init()).DoesNothing().Once();
        }

        [TestMethod]
        public virtual void Dispose_Service_Providers()
        {
            // Disposed all service providers.
            A.CallTo(() => _listener.Dispose()).DoesNothing().Once();
        }
    }
}
