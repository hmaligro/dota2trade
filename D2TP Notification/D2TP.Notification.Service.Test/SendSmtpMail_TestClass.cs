﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using D2TP.Commons.Entities.Http;
using D2TP.Commons.Enumerations.Entities.Notification;

namespace D2TP.Notification.Service.Test
{
    [TestClass]
    public class SendSmtp_TestClass : Fake_IMessenger_TestClass
    {
        [TestMethod]
        public override void SendEmail_By_SmtpClient()
        {
            // Test to send email message
            var result = JobFactory<MailRequest>.SendJobNotification(new MailRequest()
            {
                ID = "jobnotification1",
                To = @"jtagalog_ph@yahoo.com",
                From = @"developers@d2tp.com",
                DateCreated = DateTime.Now,
                MessagingPriority = Priority.Normal,
                Type = Messaging.Email,
                BodyHtml = "Hello World Test Body",
                Subject = "Hello World Subject"
            });

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SendSms_By_SmtpClient()
        {
            // Test to send sms message.
            var result = JobFactory<SmsRequest>.SendJobNotification(new SmsRequest()
            {
                ID = "jobnotification2",
                To = "0142693889",
                DateCreated = DateTime.Now,
                MessagingPriority = Priority.Normal,
                Type = Messaging.SMS,
                BodyHtml = "Hello World Test Body",
                Subject = "Hello World Subject"
            });
        }
    }
}
