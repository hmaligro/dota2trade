﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NServiceBus;
using D2TP.Configuration;

namespace D2TP.Notification.Signaler
{
    public interface ISignaler<T>
    {
        void SendMessage(string queueName, T message);
    }

    internal class Signaler<T> : ISignaler<T>, INeedInitialization
    {
        private IBus SeriesLiner { get; set; }

        public Signaler()
        {
          
        }

        public virtual void SendMessage(string queueName, T message)
        {
            SeriesLiner.Send(new Address(queueName, ConfigurationFactory.MsmqHost), message);
        }

        public void Customize(BusConfiguration configuration)
        {
            if(configuration == null)
            {
                configuration = new BusConfiguration();
                configuration.UsePersistence<InMemoryPersistence>();
                configuration.LoadMessageHandlers<Handler<T>>(new First<Handler<T>>());
                configuration.UseTransport<MsmqTransport>();
                configuration.UseSerialization<XmlSerializer>();
                configuration.EndpointName(ConfigurationFactory.MsmqEndpoint);
                configuration.PurgeOnStartup(false);
                configuration.Transactions();
                configuration.AutoSubscribe().DoNotAutoSubscribeSagas();
                configuration.AutoSubscribe().AutoSubscribePlainMessages();
                configuration.EnableInstallers();
            }

            SeriesLiner = Bus.Create(configuration).Start();
        }
    }

}
