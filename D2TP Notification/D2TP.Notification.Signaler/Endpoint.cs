namespace D2TP.Notification.Signaler
{
    using System;
    using NServiceBus;
    using D2TP.Configuration;

    internal class Endpoint<T> : IConfigureThisEndpoint, AsA_Server
    {

        public Endpoint()
        {
            Listener = new Handler<T>();
        }

        public Handler<T> Listener { get; set; }

        public void Customize(BusConfiguration configuration)
        {
            if (configuration == null)
            {
                configuration = new BusConfiguration();
                configuration.UsePersistence<InMemoryPersistence>();
                configuration.LoadMessageHandlers<Handler<T>>(new First<Handler<T>>());
                configuration.UseTransport<MsmqTransport>();
                configuration.UseSerialization<XmlSerializer>();
                configuration.EndpointName(ConfigurationFactory.MsmqEndpoint);
                configuration.PurgeOnStartup(false);
                configuration.Transactions();
                configuration.AutoSubscribe().DoNotAutoSubscribeSagas();
                configuration.AutoSubscribe().AutoSubscribePlainMessages();
            }

            var bus = Bus.Create(configuration);

            Listener.Bus.Subscribe<T>();
            Listener.Bus = bus.Start();
        }

    }

}
