﻿namespace D2TP.Notification.Signaler
{
    public static class Builder
    {
        public static ISignaler<T> CreateBus<T>()
        {
            return new Signaler<T>();
        }

        public static Handler<T> CreateHandler<T>()
        {
            var endpoint = new Endpoint<T>();
            endpoint.Customize(new NServiceBus.BusConfiguration());
            return endpoint.Listener;
        }
    }
}
