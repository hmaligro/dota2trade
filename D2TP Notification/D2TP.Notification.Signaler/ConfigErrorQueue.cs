﻿namespace D2TP.Notification.Signaler
{
    using NServiceBus.Config;
    using NServiceBus.Config.ConfigurationSource;

    internal class ConfigErrorQueue : IProvideConfiguration<MessageForwardingInCaseOfFaultConfig>
    {
        public MessageForwardingInCaseOfFaultConfig GetConfiguration()
        {
            return new MessageForwardingInCaseOfFaultConfig
            {
                ErrorQueue = "error"
            };
        }
    }

}
