﻿using System;
using NServiceBus;

namespace D2TP.Notification.Signaler
{
    [Serializable]
    public abstract class Message : ICommand
    {
        public abstract int ID { get; set; }
        public abstract bool Received { get; set; }
        public abstract DateTime? DateReceived { get; set; }
        public abstract DateTime DateCreated { get; set; }
    }

}
