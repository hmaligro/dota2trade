﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;

namespace D2TP.Notification.Signaler
{
    public class Handler<T> : IHandleMessages<T>
    {
        public virtual IBus Bus { get; set; }

        public virtual void Handle(T message)
        {
            
        }

    }
}
