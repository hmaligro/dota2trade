﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NServiceBus;

namespace D2TP.Notification.Signaler.Test
{
    /// <summary>
    /// Summary description for SeviceBus_TestClass
    /// </summary>
    [TestClass]
    public class SeviceBus_TestClass
    {
        public SeviceBus_TestClass()
        {

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private ISignaler<ConcreteMessage> _signaler;

        [TestInitialize]
        public virtual void ServiceBus_TestInitialize()
        {
            _signaler = Builder.CreateBus<ConcreteMessage>();
            Handler<ConcreteMessage> handler =  Builder.CreateHandler<ConcreteMessage>();
        }

        [TestMethod]
        public void Send_Message_To_Endpoint()
        {
            _signaler.SendMessage("d2tp", new ConcreteMessage { DateCreated = DateTime.Now });
        }

        [Serializable]
        private class ConcreteMessage : Message
        {
            public override DateTime DateCreated { get; set; }

            public override DateTime? DateReceived { get; set; }

            public override int ID { get; set; }

            public override bool Received { get; set; }
        }

    }

}
