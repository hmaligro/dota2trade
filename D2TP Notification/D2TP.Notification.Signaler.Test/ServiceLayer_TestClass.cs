﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NServiceBus;

namespace D2TP.Notification.Signaler.Test
{
    /// <summary>
    /// Summary description for ServiceLayer_TestClass
    /// </summary>
    [TestClass]
    public class ServiceLayer_TestClass
    {
        public ServiceLayer_TestClass()
        {

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Testing for the handler and message.
        /// </summary>
        [TestMethod]
        public virtual void Service_Layer_And_Message_Handler()
        {
            NServiceBus.Testing.Test.Initialize();
            NServiceBus.Testing.Test.Handler<ConcreteHandler>()
                .ExpectReply<Message>(m => m.Received)
                .OnMessage<ConcreteMessage>(m => m.Received = true);
        }

        private class ConcreteMessage : Message
        {
            public override DateTime DateCreated { get; set; }

            public override DateTime? DateReceived { get; set; }

            public override int ID { get; set; }

            public override bool Received { get; set; }
        }

        private class ConcreteHandler : Handler<ConcreteMessage>
        {
            public override IBus Bus { get; set; }

            public override void Handle(ConcreteMessage message)
            {
                var address = Bus.CurrentMessageContext.ReplyToAddress;
                Bus.Reply<ConcreteMessage>(m => m.Received = message.Received);
            }
        }
    }
}
