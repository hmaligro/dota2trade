﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Commons.Entities.Http
{
    public abstract class Request
    {
        /// <summary>
        ///  Gets or sets unique identifier per request.
        /// </summary>
        public abstract string ID { get; set; }
    }
}
