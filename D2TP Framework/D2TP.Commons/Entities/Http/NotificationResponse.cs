﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Commons.Entities.Http
{
    /// <summary>
    /// Class that handles response from an email request.
    /// </summary>
    public class NotificationResponse : Response
    {
        /// <summary>
        /// Gets or sets if request is OK and got no problem.
        /// </summary>
        public override bool IsOK { get; set; }

        /// <summary>
        /// Gets or sets the message after a resulting request.
        /// </summary>
        public override string Message { get; set; }

        /// <summary>
        /// Gets or sets the date-time the email request has sent.
        /// </summary>
        public DateTime? DateSent { get; set; }

        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public override string ID { get; set; }
    }
}
