﻿using System;

namespace D2TP.Commons.Entities.Http
{
    /// <summary>
    /// Abstract class that handles the response of each request been made.
    /// </summary>
    public abstract class Response
    {
        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public abstract string ID { get; set; }

        /// <summary>
        /// Gets or sets if request is OK and got no problem.
        /// </summary>
        public abstract bool IsOK { get; set; }

        /// <summary>
        /// Gets or sets the message after a resulting request.
        /// </summary>
        public abstract string Message { get; set; }
    }
}
