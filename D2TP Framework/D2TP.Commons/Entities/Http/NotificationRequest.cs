﻿using System;
using D2TP.Commons.Enumerations.Entities.Notification;

namespace D2TP.Commons.Entities.Http
{
    /// <summary>
    /// Abstract class that handle the request for each created Job.
    /// </summary>
    public abstract class NotificationRequest : Request
    {

        /// <summary>
        /// Gets or sets the subject/title of the created request.
        /// </summary>
        public abstract string Subject { get; set; }

        /// <summary>
        /// Gets or sets the date the request is created.
        /// </summary>
        public abstract DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the priority of request.
        /// { High, Normal }
        /// </summary>
        public abstract Priority MessagingPriority { get; set; }

        /// <summary>
        /// Gets or sets the messaging type of the request
        /// { Email, Sms, Alarm }
        /// </summary>
        public abstract Messaging Type { get; set; }

        /// <summary>
        /// Gets or sets the content of the created request
        /// </summary>
        public abstract string BodyHtml { get; set; }

        /// <summary>
        /// Gets or sets sender of the created request.
        /// </summary>
        public abstract string From { get; set; }

        /// <summary>
        /// Gets or sets receiver of the created request.
        /// </summary>
        public abstract string To { get; set; }

    }

    #region notification

    /// <summary>
    /// Class that handles sms messages request from client.
    /// </summary>
    public class SmsRequest : NotificationRequest
    {
        /// <summary>
        /// Gets or sets the content of the created request
        /// </summary>
        public override string BodyHtml { get; set; }
    
        /// <summary>
        /// Gets or sets the date the request is created.
        /// </summary>
        public override DateTime DateCreated { get; set; }
     
        /// <summary>
        /// Gets or sets sender of the created request.
        /// </summary>
        public override string From { get; set; }
     
        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public override string ID { get; set; }
     
        /// <summary>
        /// Gets or sets the priority of request.
        /// { High, Normal }
        /// </summary>
        public override Priority MessagingPriority { get; set; }
     
        /// <summary>
        /// Gets or sets the subject/title of the created request.
        /// </summary>
        public override string Subject { get; set; }

        /// <summary>
        /// Gets or sets receiver of the created request.
        /// </summary>
        public override string To { get; set; }
      
        /// <summary>
        /// Gets or sets the messaging type of the request
        /// { Email, Sms, Alarm }
        /// </summary>
        public override Messaging Type { get; set; }
    }

    /// <summary>
    /// Class that handles mail message request from client.
    /// </summary>
    public class MailRequest : NotificationRequest
    {

        /// <summary>
        /// Gets or sets the carbon copy.
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// Gets or sets the bcc of the created request.
        /// </summary>
        public string Bcc { get; set; }

        /// <summary>
        /// Gets or sets the display name of sender.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the date the request is created.
        /// </summary>
        public override DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the priority of request.
        /// { High, Normal }
        /// </summary>
        public override Priority MessagingPriority { get; set; }

        /// <summary>
        /// Gets or sets the messaging type of the request
        /// { Email, Sms, Alarm }
        /// </summary>
        public override Messaging Type { get; set; }

        /// <summary>
        /// Gets or sets the content of the created request
        /// </summary>
        public override string BodyHtml { get; set; }

        /// <summary>
        ///  Gets or sets sender of the created request.
        /// </summary>
        public override string From { get; set; }

        /// <summary>
        /// Gets or sets receiver of the created request.
        /// </summary>
        public override string To { get; set; }

        /// <summary>
        /// Gets or sets unique identifier per request.
        /// </summary>
        public override string ID { get; set; }

        /// <summary>
        /// Gets or sets the subject/title of the created request.
        /// </summary>
        public override string Subject { get; set; }
    }

    #endregion
}
