﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Commons.Extensions
{
    public static class DecimalExtensions
    {

        /// <summary>
        /// Converts object to integer value. A default value will be provided incase failed to convert. 
        /// </summary>
        /// <param name="obj">Current object value.</param>
        /// <param name="value">Default value returned.</param>
        /// <returns></returns>
        public static int AsInt(this object obj, int value = 0)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch
            {
                return value;
            }
        }

        /// <summary>
        /// Converts object to long value. A default value will be provided incase failed to convert. 
        /// </summary>
        /// <param name="obj">Current object value.</param>
        /// <param name="value">Default value returned.</param>
        /// <returns></returns>
        public static long AsLong(this object obj, long value = 0)
        {
            try
            {
                return Convert.ToInt64(obj);
            }
            catch
            {
                return value;
            }
        }

     
    }
}
