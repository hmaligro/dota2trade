﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Commons.Extensions
{
    public static class BooleanExtensions
    {
        /// <summary>
        /// Converts object to a boolean value. A default value will be provided incase failed to convert. 
        /// </summary>
        /// <param name="obj">Current object value.</param>
        /// <param name="value">Default value returned.</param>
        /// <returns></returns>
        public static bool AsBool(this object obj, bool value = false)
        {
            try
            {
                return Convert.ToBoolean(obj);
            }
            catch
            {
                return value;
            } 
        }
    }
}
