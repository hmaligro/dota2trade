﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Commons.Enumerations.Entities.UserManagement
{
    public enum UserStatus
    {
        Active,
        NotActive,
        Blacklisted
    }
}
