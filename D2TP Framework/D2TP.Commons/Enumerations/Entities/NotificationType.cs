﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2TP.Commons.Enumerations.Entities.Notification
{
    public enum NotificationType
    {
        MemberVerification,
        WelcomeMember,
        WelcomeUser,
        PostCourier

    }
}
