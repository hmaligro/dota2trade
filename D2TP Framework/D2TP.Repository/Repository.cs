﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Reflection;
using D2TP.Commons.Enumerations.DAL;
using D2TP.Commons.Extensions;
using D2TP.DAL;
using EntityFramework.Extensions;

namespace D2TP.Repository
{
    internal class Repository : IRepository
    {
        private DbContext Context { get; set; }
        private DbContextTransaction Transaction { get; set; }

        public Repository()
        {
            try
            {
                this.Context = new D2tpContext();
                this.Context.Database.Initialize(true);

                Status = ConnectionStatus.Connected;
            }
            catch
            {
                Status = ConnectionStatus.NotConnected;
            }
        }

        #region create

        /// <summary>
        /// Saves a model object information to a datasource.
        /// </summary>
        /// <param name="model">The model object.</param>
        public void Save<TEntity>(TEntity model) where TEntity : class
        {
            Attach<TEntity>(model);
        }

        #endregion

        #region read

        /// <summary>
        /// Finds the first entity in the records and returns default if nothing is found.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public TEntity First<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            return FirstDefault<TEntity>(criteria);
        }

        /// <summary>
        /// Returns the single record matching the given criteria and throws an exception if more than one records are found.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <returns></returns>
        public TEntity Single<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            return SelectSingle<TEntity>(criteria);
        }

        /// <summary>
        /// Finds a record from a datasource matching the given keys.
        /// </summary>
        /// <param name="keys">The filter key.</param>
        /// <returns></returns>
        public TEntity Find<TEntity>(params object[] keys) where TEntity : class
        {
            return FindKeys<TEntity>(keys);
        }

        /// <summary>
        /// Gets all collection records from a datasource.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TEntity> All<TEntity>() where TEntity : class
        {
            return GetCollection<TEntity>();
        }

        /// <summary>
        /// Gets all collection records from datasource based on a given criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <returns></returns>
        public IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            return GetCollection<TEntity>(criteria);
        }

        /// <summary>
        /// Returns true if record exist based on the given key.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exist<TEntity>(Predicate<TEntity> match) where TEntity : class
        {
            return DoesExist<TEntity>(match);
        }

        /// <summary>
        /// Retrieves record/s from a datasource with criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="limit">Limited number of records retrieved.</param>
        /// <param name="where"></param>
        /// <returns></returns>
        public IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria, int limit) where TEntity : class
        {
            return GetCollection<TEntity>(criteria, limit);
        }

        /// <summary>
        /// Retrieves record/s from a datasource with criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="skip">Slipped number of records</param>
        /// <param name="limit">Limited number of records retrieved.</param>
        /// <returns></returns>
        public IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria, int skip, int limit) where TEntity : class
        {
            return GetCollection<TEntity>(criteria, skip, limit);
        }

        /// <summary>
        /// Retrieves record/s from a datasource with criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="skip">Slipped number of records</param>
        /// <param name="limit">Limited number of records retrieved.</param>
        /// <param name="isAscending">Is sorted as ascending order.</param>
        /// <param name="sort">Array of column names to be sorted.</param>
        /// <returns></returns>
        public IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria, int skip, int limit, bool? isAscending, Expression<Func<TEntity, string>> sort) where TEntity : class
        {
            return GetCollection<TEntity>(criteria, skip, limit, isAscending, sort);
        }

        #endregion

        #region update

        /// <summary>
        /// Find and update fields in table.
        /// </summary>
        /// <param name="model">Used Func<> or a Lamda Expression.</param>
        /// <param name="properties">Parameter that contains the properties to be updated.</param>
        /// <returns></returns>
        public TEntity FindUpdate<TEntity>(TEntity model, params Expression<Func<TEntity, string>>[] properties) where TEntity : class
        {
            return FindModify<TEntity>(model, properties);
        }

        /// <summary>
        /// Find and update fields in table.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="properties">Parameter that contains the properties to be updated.</param>
        /// <returns></returns>
        public int FindUpdate<TEntity>(Expression<Func<TEntity, bool>> criteria, Expression<Func<TEntity, TEntity>> updateExpression) where TEntity : class
        {
            return FindModify<TEntity>(criteria, updateExpression);
        }

        #endregion

        #region delete

        /// <summary>
        /// Deletes certain number of records.
        /// </summary>
        /// <param name="criteria">Ctriteria given to delete records.</param>
        public void Delete<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            Remove<TEntity>(criteria);
        }

        /// <summary>
        /// Deletes a single record.
        /// </summary>
        /// <param name="model">The object model.</param>
        public void Delete<TEntity>(TEntity model) where TEntity : class
        {
            Remove<TEntity>(model);
        }

        #endregion

        /// <summary>
        /// Gets the connection status between repository and database.
        /// </summary>
        public ConnectionStatus Status { get; private set; }

        /// <summary>
        /// Commits all the changes done in the repository.
        /// </summary>
        public void CommitChanges()
        {
            if (this.Context != null)
                this.Context.SaveChanges();
        }

        /// <summary>
        /// Disposes the instance of database context.
        /// </summary>
        public void Dispose()
        {
            if (this.Context != null)
                this.Context.Dispose();
        }

        /// <summary>
        /// Creates a new trasactional implementation.
        /// </summary>
        public void BeginTransaction()
        {
            // Open a new connection.
            if (this.Context.Database.Connection.State != System.Data.ConnectionState.Open)
                this.Context.Database.Connection.Open();


            // Begins the transaction.
            this.Transaction = this.Context.Database.BeginTransaction();
        }

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        public void CommitTransaction()
        {
            // Commits the transaction.
            if (this.Transaction != null)
                this.Transaction.Commit();

            // Closing databse connection.
            if (this.Context.Database.Connection.State == System.Data.ConnectionState.Open)
                this.Context.Database.Connection.Close();

            this.Transaction = null;
        }

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        public void RollbackTransaction()
        {
            // Rollbacks the transaction.
            if (this.Transaction != null)
                this.Transaction.Rollback();

            // Closing database connection.
            if (this.Context.Database.Connection.State == System.Data.ConnectionState.Open)
                this.Context.Database.Connection.Close();

            this.Transaction = null;
        }


        #region helpers

        private void Attach<TEntity>(TEntity model) where TEntity : class
        {
            Type t = typeof(TEntity);
            PropertyInfo prop = t.GetProperty("ID");

            var entity = this.Context.Set<TEntity>().Attach(model);
            this.Context.Entry<TEntity>(model).State = prop.GetValue(model).AsInt(0) == 0 ? EntityState.Added : EntityState.Modified;
        }
        private TEntity FirstDefault<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            return this.Context.Set<TEntity>().FirstOrDefault<TEntity>(criteria);
        }
        private TEntity SelectSingle<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            return this.Context.Set<TEntity>().Single<TEntity>(criteria);
        }
        private TEntity FindKeys<TEntity>(params object[] keys) where TEntity : class
        {
            return this.Context.Set<TEntity>().Find(keys);
        }
        private bool DoesExist<TEntity>(Predicate<TEntity> match) where TEntity : class
        {
            return this.Context.Set<TEntity>().ToList<TEntity>().Exists(match);
        }
        private IEnumerable<TEntity> GetCollection<TEntity>() where TEntity : class
        {
            return this.Context.Set<TEntity>().AsEnumerable();
        }
        private IQueryable<TEntity> GetCollection<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            return this.Context.Set<TEntity>().Where<TEntity>(criteria);
        }
        private IQueryable<TEntity> GetCollection<TEntity>(Expression<Func<TEntity, bool>> criteria, int limit) where TEntity : class
        {
            return GetCollection<TEntity>(criteria).Take<TEntity>(limit);
        }
        private IQueryable<TEntity> GetCollection<TEntity>(Expression<Func<TEntity, bool>> criteria, int skip, int limit) where TEntity : class
        {
            return GetCollection<TEntity>(criteria).Take<TEntity>(limit).Skip<TEntity>(skip);
        }
        private IOrderedQueryable<TEntity> GetCollection<TEntity>(Expression<Func<TEntity, bool>> criteria, int skip, int limit, bool? isAscending, Expression<Func<TEntity, string>> sort) where TEntity : class
        {
            var entities = GetCollection<TEntity>(criteria).Take<TEntity>(limit).Skip<TEntity>(skip);
            return isAscending.HasValue && isAscending.Value ? entities.OrderBy<TEntity, string>(sort) : entities.OrderByDescending<TEntity, string>(sort);
        }
        private TEntity FindModify<TEntity>(TEntity model, params Expression<Func<TEntity, string>>[] properties) where TEntity : class
        {
            this.Context.Set<TEntity>().Attach(model);

            var entry = this.Context.Entry<TEntity>(model);
            properties.ToList().ForEach(p => entry.Property((p as Expression<Func<TEntity, string>>)).IsModified = true);

            return model;
        }
        private int FindModify<TEntity>(Expression<Func<TEntity, bool>> criteria, Expression<Func<TEntity, TEntity>> updateExpression) where TEntity : class
        {
            var entities = GetCollection<TEntity>(criteria);
            return entities.Update<TEntity>(updateExpression);
        }
        private void Remove<TEntity>(TEntity model) where TEntity : class
        {
            this.Context.Set<TEntity>().Remove(model);
            this.Context.Entry<TEntity>(model).State = System.Data.Entity.EntityState.Deleted;
        }
        private void Remove<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class
        {
            var entities = GetCollection<TEntity>(criteria);
            this.Context.Set<TEntity>().RemoveRange(entities);
        }

        #endregion

    }
}
