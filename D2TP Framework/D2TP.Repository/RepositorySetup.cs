﻿using Autofac;

namespace D2TP.Repository
{
    public class RepositorySetup
    {
        public static IRepository Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Repository>().As<IRepository>().Named<IRepository>(Configuration.ConfigurationFactory.ServiceName).InstancePerLifetimeScope();
            var container = builder.Build();
            return container.Resolve<IRepository>();
        }
    }
}
