﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using D2TP.Commons.Enumerations.DAL;

namespace D2TP.Repository
{
    public interface IRepository : IDisposable
    {
        /// <summary>
        /// Gets the connection status between repository and database.
        /// </summary>
        ConnectionStatus Status { get; }
        /// <summary>
        /// Commits all the changes done in the repository.
        /// </summary>
        void CommitChanges();
        /// <summary>
        /// Creates a new trasactional implementation.
        /// </summary>
        void BeginTransaction();
        /// <summary>
        /// Commits the transaction.
        /// </summary>
        void CommitTransaction();
        /// <summary>
        /// Rollbacks the trasaction.
        /// </summary>
        void RollbackTransaction();

        /// Saves a model object information to a datasource.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="model">The object model.</param>
        /// <returns>The ID of the attached entity</returns>
        void Save<TEntity>(TEntity model) where TEntity : class;
        /// <summary>
        /// Finds the first entity in the records and returns default if nothing is found.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="criteria"></param>
        /// <returns></returns>
        TEntity First<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;
        /// <summary>
        /// Finds a record from a datasource matching the given keys.
        /// </summary>
        /// <param name="keys">The filter key.</param>
        /// <returns></returns>
        TEntity Find<TEntity>(params object[] keys) where TEntity : class;
        /// <summary>
        /// Selects a single record from a datasource matching the given criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <returns></returns>
        TEntity Single<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;
        /// <summary>
        /// Gets all collection records from a datasource.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> All<TEntity>() where TEntity : class;
        /// <summary>
        /// Gets all collection records from datasource based on a given criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <returns></returns>
        IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;
        /// <summary>
        /// Retrieves record/s from a datasource with criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="limit">Limited number of records retrieved.</param>
        /// <param name="where"></param>
        /// <returns></returns>
        IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria, int limit) where TEntity : class;
        /// <summary>
        /// Retrieves record/s from a datasource with criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="skip">Slipped number of records</param>
        /// <param name="limit">Limited number of records retrieved.</param>
        /// <returns></returns>
        IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria, int skip, int limit) where TEntity : class;
        /// <summary>
        /// Retrieves record/s from a datasource with criteria.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="skip">Slipped number of records</param>
        /// <param name="limit">Limited number of records retrieved.</param>
        /// <param name="isAscending">Is sorted as ascending order.</param>
        /// <param name="sort">Array of column names to be sorted.</param>
        /// <returns></returns>
        IQueryable<TEntity> All<TEntity>(Expression<Func<TEntity, bool>> criteria, int skip, int limit, bool? isAscending, Expression<Func<TEntity, string>> sort) where TEntity : class;
        /// <summary>
        /// Returns true if record exist based on the given key.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Exist<TEntity>(Predicate<TEntity> match) where TEntity : class;
        /// <summary>
        /// Find and update certain fields in table.
        /// </summary>
        /// <param name="model">Used Func<> or a Lamda Expression.</param>
        /// <param name="properties">Parameter that contains the properties to be updated.</param>
        /// <returns></returns>
        TEntity FindUpdate<TEntity>(TEntity model, params Expression<Func<TEntity, string>>[] properties) where TEntity : class;
        /// <summary>
        /// Find and update certain fields in table.
        /// </summary>
        /// <param name="criteria">Used Func<> or a Lamda Expression.</param>
        /// <param name="updateExpression">Parameter that contains update expression.</param>
        /// <returns>Returns number of entities affected.</returns>
        int FindUpdate<TEntity>(Expression<Func<TEntity, bool>> criteria, Expression<Func<TEntity, TEntity>> updateExpression) where TEntity : class;
        /// <summary>
        /// Deletes a single record.
        /// </summary>
        /// <param name="model">The object model.</param>
        void Delete<TEntity>(TEntity model) where TEntity : class;
        /// <summary>
        /// Deletes certain number of records.
        /// </summary>
        /// <param name="criteria">Ctriteria given to delete records.</param>
        void Delete<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;

    }
}
