﻿using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace D2TP.DAL.Test
{
    using D2TP.Repository;
    using D2TP.Models;

    /// <summary>
    /// Summary description for CRUDFindAndModifyWithCleanUp
    /// </summary>
    [TestClass]
    public class UpdateWithCleanUp
    {
        public UpdateWithCleanUp()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private int ID1 { get; set; }
        private int ID2 { get; set; }

        /// <summary>
        /// Intializes test by adding 2 new records inside a trasactional implementation.
        /// </summary>
        [TestInitialize]
        public void Find_Update_TestInitialize()
        {
            var repo = RepositorySetup.Setup();

            try
            {
                repo.BeginTransaction();

                #region user1

                // Add first user.
                var user1 = new User
                {
                    Email = "jtagalog.ph@gmail.com",
                    Firstname = "Anthony Jidd",
                    Lastname = "Tagalog",
                    Mobile = "0142693889",
                    DateCreated = new DateTime(2015, 11, 23),
                    LanguageID = 1,
                    Username = "D2tpUser1",
                    Password = "P@ssw0rd"
                };

                // Adds a new user record.
                repo.Save<User>(user1);
                repo.CommitChanges();

                this.ID1 = user1.ID;

                Assert.IsTrue(this.ID1 > 0);

                #endregion

                #region user2

                // Add second user.
                var user2 = new User
                {
                    Email = "jtagalog_ph@yahoo.com",
                    Firstname = "Alexandralleria",
                    Lastname = "Tagalog",
                    Mobile = "12345678",
                    DateCreated = DateTime.Now,
                    LanguageID = 1,
                    Username = "D2tpUser1",
                    Password = "P@ssw0rd"
                };

                // Adds a new user record.
                repo.Save<User>(user2);
                repo.CommitChanges();

                this.ID2 = user2.ID;

                Assert.IsTrue(this.ID2 > 0);

                #endregion

                repo.CommitTransaction();
            }
            catch (Exception ex)
            {
                repo.RollbackTransaction();
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Tests find and update existing user record.
        /// </summary>
        [TestMethod]
        public void Update_User_TestMethod()
        {
            var repo = RepositorySetup.Setup();

            try
            {
                var user = repo.Single<User>(u => u.ID == this.ID1);
                user.Firstname = "Alleria";

                var affected = repo.FindUpdate<User>(user, new Expression<Func<User, string>>[] { u => u.Firstname });
                repo.CommitChanges();

                Assert.IsTrue(affected != null);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Tests find and update existing user record with criteria.
        /// </summary>
        [TestMethod]
        public void Update_Users_With_Criteria_TestMethod()
        {
            var repo = RepositorySetup.Setup();

            try
            {
                var affected = repo.FindUpdate<User>(u => u.ID == this.ID2, p => new User { Firstname = "Ivyrez" });
                Assert.IsTrue(affected > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Test cleanup for update user record.
        /// </summary>
        [TestCleanup]
        public void TestCleanUp()
        {
            var repo = RepositorySetup.Setup();

            try
            {
                // Checks if the record does still exist before deleting.
                Assert.IsTrue(repo.Exist<User>(u => u.ID == this.ID1));
                Assert.IsTrue(repo.Exist<User>(u => u.ID == this.ID2));

                // Deletes the user.
                repo.Delete<User>(u => u.ID == this.ID1);
                repo.Delete<User>(u => u.ID == this.ID2);
                repo.CommitChanges();

                // Checks if the record does not exist anymore.
                Assert.IsTrue(!repo.Exist<User>(u => u.ID == this.ID1));
                Assert.IsTrue(!repo.Exist<User>(u => u.ID == this.ID2));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
