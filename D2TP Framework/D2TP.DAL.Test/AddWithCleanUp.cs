﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace D2TP.DAL.Test
{
    using D2TP.Repository;
    using D2TP.Models;
    using D2TP.Commons.Enumerations.DAL;

    /// <summary>
    /// Summary description for Repository_TestClass
    /// </summary>
    [TestClass]
    public class AddWithCleanUp
    {
        public AddWithCleanUp()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private int ID { get; set; }

        /// <summary>
        /// Initializes repository test to create a new database and inserting seed data.
        /// </summary>
        [TestInitialize]
        public void AddUser_TestInitialize()
        {
            var repo = RepositorySetup.Setup();

            if (repo.Status != ConnectionStatus.Connected)
                Assert.Fail(repo.Status.ToString());
        }

        /// <summary>
        /// Test to add new user record.
        /// </summary>
        [TestMethod]
        public void Add_User_TestMethod()
        {
            var repo = RepositorySetup.Setup();

            try
            {
                var user = new User
                {
                    Email = "jtagalog.ph@gmail.com",
                    Firstname = "Anthony Jidd",
                    Lastname = "Tagalog",
                    Mobile = "0142693889",
                    DateCreated = new DateTime(2015, 11, 23),
                    LanguageID = 1,
                    Username = "D2tpUser1",
                    Password = "P@ssw0rd"
                };

                // Adds a new user record.
                repo.Save<User>(user);
                repo.CommitChanges();

                this.ID = user.ID;

                Assert.IsTrue(this.ID > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Test to remove the newly added user record.
        /// </summary>
        [TestCleanup]
        public void TestCleanUp()
        {
            var repo = RepositorySetup.Setup();

            try
            {
                // Checks if the record does still exist before deleting.
                Assert.IsTrue(repo.Exist<User>(u => u.ID == ID));

                // Deletes the user.
                repo.Delete<User>(u => u.ID == ID);
                repo.CommitChanges();

                // Checks if the record does not exist anymore.
                Assert.IsTrue(!repo.Exist<User>(u => u.ID == ID));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

    }

}
