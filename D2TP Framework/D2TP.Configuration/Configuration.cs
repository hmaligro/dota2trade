﻿using System.Configuration;
using D2TP.Commons.Extensions;

namespace D2TP.Configuration
{
    public static class ConfigurationFactory
    {
        #region Dal Configurations

        /// <summary>
        /// Gets the connection string used to connect to the database.
        /// </summary>
        public static string SqlConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["Sql-Server"].ToString();
            }
        }

        /// <summary>
        /// Gets the service name used to register the single instance of the repository.
        /// </summary>
        public static string ServiceName
        {
            get
            {
                return ConfigurationManager.AppSettings["ServiceName"].ToString();
            }
        }

        #endregion

        #region Smtp Configurations

        /// <summary>
        /// Gets configuration smtp server.
        /// </summary>
        public static string SmtpServer
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpServer"].ToString();
            }
        }

        /// <summary>
        /// Gets configuration username.
        /// </summary>
        public static string SmtpUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpUsername"].ToString();
            }
        }

        /// <summary>
        /// Gets configuration password.
        /// </summary>
        public static string SmtpPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpPassword"].ToString();
            }
        }

        /// <summary>
        /// Gets configuration domain.
        /// </summary>
        public static string SmtpDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpDomain"].ToString();
            }
        }

        /// <summary>
        /// Gets configuration ssl.
        /// </summary>
        public static bool SmtpEnableSsl
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpEnableSsl"].AsBool(false);
            }
        }

        /// <summary>
        /// Gets configuration port.
        /// </summary>
        public static int SmtpPort
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpPort"].AsInt(8080);
            }
        }

        /// <summary>
        /// Gets configuration of ssl port.
        /// </summary>
        public static int SmtpPortSsl
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpPortSsl"].AsInt(8080);
            }
        }

        /// <summary>
        /// Gets configuration timeout.
        /// </summary>
        public static int SmtpTimeout
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpTimeout"].AsInt(10000);
            }
        }

        #endregion

        #region ServiceBus Configurations

        public static string MsmqEndpoint
        {
            get
            {
                return ConfigurationManager.AppSettings["MsmqEndpoint"].ToString();
            }
        }

        public static string MsmqHost
        {
            get
            {
                return ConfigurationManager.AppSettings["MsmqHost"].ToString();
            }
        }

        #endregion

    }
}
