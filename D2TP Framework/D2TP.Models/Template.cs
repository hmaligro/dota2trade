﻿using System.ComponentModel.DataAnnotations;
using D2TP.Commons.Enumerations.Entities.Notification;

namespace D2TP.Models
{
    public class Template 
    {
        [Required]
        public string Subject { get; set; }
        [Required]
        public string From { get; set; }
        [Required]
        public string HtmlBody { get; set; }
        [Required]
        public NotificationType Type { get; set; }
        [Required]
        public Messaging Messaging { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Key]
        public int ID { get; set; }

        public Language Language { get; set; }
    }
}
