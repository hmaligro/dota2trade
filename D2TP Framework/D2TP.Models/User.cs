﻿using System;
using System.ComponentModel.DataAnnotations;
using D2TP.Commons.Enumerations.Entities.UserManagement;

namespace D2TP.Models
{
    public class User
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Mobile { get; set; }
        public byte[] Avatar { get; set; }
        public string StreamID { get; set; }
        [Required]
        public int LanguageID { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        public string VerificationCode { get; set; }
        public bool? IsAccountVerified { get; set; }
        public bool? IsMobileVerified { get; set; }
        [Required]
        public UserStatus Status { get; set; }

        public Language Language { get; set; }
    }
}
