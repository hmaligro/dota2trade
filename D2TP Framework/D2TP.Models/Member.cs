﻿using System;

namespace D2TP.Models
{
    public class Member
    {
       
        public int ID { get; set; }

        public User User { get; set; }

        public Language Language { get; set; }

    }
}
