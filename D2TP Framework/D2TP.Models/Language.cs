﻿using System.ComponentModel.DataAnnotations;

namespace D2TP.Models
{
    public class Language
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string CultureResx { get; set; }
        [Key]
        public int ID { get; set; }
    }
}
