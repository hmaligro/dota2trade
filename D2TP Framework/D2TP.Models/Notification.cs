﻿using System;
using System.ComponentModel.DataAnnotations;
using D2TP.Commons.Enumerations.Entities.Notification;

namespace D2TP.Models
{
    public class Notification 
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        public DateTime? DateSent { get; set; }
        [Required]
        public Messaging Type { get; set; }
        [Required]
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string BodyHtml { get; set; }
        public string BodyPlainText { get; set; }
        [Required]
        public string From { get; set; }
        public string DisplayName { get; set; }
        [Required]
        public Priority MessagingPriority { get; set; }

    }
}
