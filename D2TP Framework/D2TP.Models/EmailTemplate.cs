﻿namespace D2TP.Models
{
    public class EmailTemplate : Template
    {
        public string SenderDisplayName { get; set; }
    }
}
