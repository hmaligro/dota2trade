﻿using System.Data.Entity;
using D2TP.Configuration;
using D2TP.Models;

namespace D2TP.DAL
{
    /// <summary>
    /// Class that manages connection to an SQL Database.
    /// </summary>
    public class D2tpContext : DbContext
    {
        public D2tpContext()
            : base(ConfigurationFactory.SqlConnectionString)
        {
            Database.SetInitializer<D2tpContext>(new D2tpDropIfModelChangesContextInitializer());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<SmsTemplate> SmsTemplates { get; set; }
        public DbSet<Template> Templates { get; set; }

    }
}
