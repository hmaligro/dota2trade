﻿using System.Collections.Generic;
using System.Data.Entity;
using D2TP.Models;

namespace D2TP.DAL
{
    /// <summary>
    /// Class that handles the implementation of seeding data.
    /// </summary>
    internal class SeedData
    {
        private D2tpContext _db;

        public SeedData(D2tpContext db)
        {
            _db = db;
        }

        public void InitData()
        {
            var languages = new List<Language>()
            {
               new Language() { ID = 1, Name="English", CultureResx="en" },
               new Language() { ID = 2, Name="Dutch", CultureResx="de" }
            };

            languages.ForEach(l => _db.Languages.Add(l));
            _db.SaveChanges();
        }
    }

    /// <summary>
    /// Class used to seed data and drop and create database if model changes.
    /// </summary>
    internal class D2tpDropIfModelChangesContextInitializer : DropCreateDatabaseIfModelChanges<D2tpContext>
    {
        protected override void Seed(D2tpContext context)
        {
            var seed = new SeedData(context);
            seed.InitData();
        }
    }

    /// <summary>
    /// Class used to seed data and create database if not existing.
    /// </summary>
    internal class D2tpCreateIfNotExistingContextInitializer : CreateDatabaseIfNotExists<D2tpContext>
    {
        protected override void Seed(D2tpContext context)
        {
            var seed = new SeedData(context);
            seed.InitData();
        }
    }
}
